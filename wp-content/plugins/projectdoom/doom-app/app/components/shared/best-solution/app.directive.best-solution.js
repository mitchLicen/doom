/**
* @public
*
*
* @App Dependencies [xhr, utils, memcache, routes, sessionservice, domReady]

* General-purpose Event binding. Bind any event not natively supported by Angular
* Pass an object with keynames for events to ui-event
* Allows $event object and $params object to be passed
*
* @example <input ui-event="{ focus : 'counter++', blur : 'someCallback()' }">
* @example <input ui-event="{ myCustomEvent : 'myEventHandler($event, $params)'}">
*
* @param ui-event {string|object literal} The event to bind to as a string or a hash of events with their callbacks
*
* @return Angular.module.appDirectiveBestSolution
*
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*	http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
**/
define(function (require, exports, module) {

	'use strict';

	require("app-xhr");
	require("app-utils");
	require("app-retailer");
	require("app-product");
	require("angular-progress");
	require("angular-material");
	require("jlinq");

	// Load dependent modules
	var appDirectiveBestSolution,
		appConfig = JSON.parse(require("text!../../app/app.config.json")),
		domReady = require("domReady"),
		isMobile = require("isMobile"),
		// jlinq = 
		angular = require("angular");

	//
	var Application = Application || {};
	Application.Directives = {};

	Application.Directives.uiBestSolution = function () {

		return {
			restrict: 'AE',
			transclude: true, // pass entire template?
			templateUrl: appConfig.general.path + '/app/components/shared/best-solution/best-solution.php',
			scope: {
				// items: '=?itemList',
				// itemLength: '=?listLength'
			},
			link: function (scope, element, attr) {
				// console.log("LINKING Best Solution!");
				// scope.preInit();
			},
			controller: ['$sce', '$filter', '$rootScope', '$scope', '$http', '$q', '$route', '$location', '$timeout', '$mdSidenav', '$log', '$mdDialog', 'transformRequestAsFormPost', 'Utils', 'ngProgress', 'retailersManager', 'productsManager', 'insectsManager', 'packagesManager', 'SessionService', function ($sce, $filter, $rootScope, $scope, $http, $q, $route, $location, $timeout, $mdSidenav, $log, $mdDialog, transformRequestAsFormPost, Utils, ngProgress, retailersManager, productsManager, insectsManager, packagesManager, SessionService) {
				$scope.sessionInfo = {};
				$scope.show = [];
				//init data
				$scope.preInit = function() {
					$scope.pest = $route.current.pathParams.ID;
					productsManager.getProducts({method: 'GET',type: 'product'}).then(function(result){
						productsManager.filterProductsByPest([{slug: $scope.pest}], result).then(function(data){
							var products = data;
							$scope.products = products;
						}, function(err){
						  	console.log("Error", err);
						});
					});
					var requestObjInsect = {
						method: 'GET',
						type: 'insect'
					};
					$scope.insects = [];
			
					insectsManager.getInsects(requestObjInsect).then(function(data){
						var insects = data;
						for (var i = 0; i < insects.length; i++) {
							insects[i].selected = '';
							if ($scope.pest.toLowerCase() == insects[i].post_name.toLowerCase()) {
								insects[i].selected = 'selected';
								$scope.pest = insects[i];
								// $scope.next();
								break;
							}
						}
						$scope.insects = insects;
					}, function(err){
						console.log("Error", err);
					});
					SessionService.checkSession().then(function(data){
					//   console.warn(data);
					  $scope.sessionInfo = data;
					  $scope.sessionInfo.firstVisit = new Date($scope.sessionInfo.firstVisit);
					  $scope.sessionInfo.lastCheck = new Date($scope.sessionInfo.lastCheck);
					  $scope.sessionInfo.lastVisit = new Date($scope.sessionInfo.lastVisit);
					//   console.warn($scope.sessionInfo);
					//   $scope.reset();
					  $scope.init();
					}, function (err){
					  console.error(err);
					});
				}
				$scope.init = function(){
					var timePassed = (new Date().getTime() - $scope.sessionInfo.firstVisit.getTime()) / 1000;
					
					$scope.days = timePassed / 86400;
					$scope.result = $scope.evaluate();
					
					for (var i = 0; i < $scope.result.stats.length; i++) {
						$scope.result.stats[i].time = $scope.days.toFixed(0);
						// $scope.result.stats[i].stat *= $scope.days;
						$scope.result.stats[i].dummy = 0;
						$scope.result.stats[i].show = "0";
					}
					// $scope.show.push(stat);
				}

				$scope.showStat = function (stat) {
					stat.formatted = $scope.outputCopy(stat);
					$scope.selectedStat = stat;
		  
					if( $scope.finalPage == "active" ) {
					  //console.log('.icon-' + stat.type);
		  
					  var allStats =  document.querySelectorAll('.stat-selector .icon' );
		  
					  for (var index = 0; index < allStats.length; index++) {
						  
						  var element = allStats[index];
		  
						  if( element.classList.contains('active') ) {
							element.classList.remove('active');
							break;
						  }
		  
					  }
					  if (stat.type != 'molts') {
						document.querySelector('.stat-selector .icon-' + stat.type ).classList.toggle('active');
					  } else {
						document.querySelector('.stat-selector .icon-' + $scope.pest.post_name ).classList.toggle('active');
					  }
		  
					}
		  
				}
		  
				$scope.outputCopy = function (stat) {
					var copy = stat.copy;
					if (stat.copy != undefined && stat.copy.indexOf("#STAT#") >= 0) {
					  // var number = Utils.numberWithCommas(stat.stat);
					  copy = copy.replace("#STAT#", "<br/><span class='stat'>"+$filter('shortNumber')(stat.stat)+"</span><br/>");
					}
					if (copy != undefined && copy.indexOf("#TIME#") >= 0) {
					  var time = "";
					  switch ($scope.days.toFixed(0)) {
						case 1:
						  time = "day";
						  break;
						default:
						  time = $scope.days.toFixed(0) + " days";
						  break;
					  }
					  copy = copy.replace("#TIME#", time);
					}
					// return $sce.trustAsHtml("<br/><span class='stat'>"+$filter('shortNumber')(stat.stat) +" "+ stat.type+"</span><br/>");
					return $sce.trustAsHtml(copy);
				}
				$scope.evaluate = function () {
					var dataPoints = {
					  eggs: parseFloat($scope.pest.doom_insect_egg),
					  blood: parseFloat($scope.pest.doom_insect_sucks),
					  molts: parseFloat($scope.pest.doom_insect_molt),
					  vomits: parseFloat($scope.pest.doom_insect_vomit),
					  poops: parseFloat($scope.pest.doom_insect_poop)
					}
					var statCopy = {
					  eggs: "Since you first visited, a single "+$scope.pest.post_name+" could have layed #STAT# eggs in your home.",
					  blood: "Since you first visited, a single "+$scope.pest.post_name+" could have sucked #STAT# blood from you or your pets.",
					  molts: "Since you first visited, a single "+$scope.pest.post_name+" could have molted #STAT# times in your home.",
					  vomits: "Since you first visited, a single "+$scope.pest.post_name+" could have vomited #STAT# times in your home.",
					  poops: "Since you first visited, a single "+$scope.pest.post_name+" could have pooped #STAT# times in your home."
					}
		  
					var results = [];
					for (var key in dataPoints){
					  if (typeof dataPoints[key] !== 'function') {
						if (!isNaN(dataPoints[key]) && dataPoints[key] != undefined ) {
						  var resultedAmount = (dataPoints[key] * $scope.days);
						  if (resultedAmount > 1) {  
							var copy = statCopy[key];
							results.push({
							  type: key,
							  copy: copy,
							  stat: resultedAmount
							});
						  }
						  // results[key]= resultedAmount;
						}
					  }
					}
					return {
					  pest: $scope.pest,
					  product: $scope.products[0],
					  days: $scope.days,
					  stats: results
					};
				}
				$scope.goto = function (path) {
					Utils.scrollWithEase(0);
					$location.path(path);
				}

				$scope.preInit();
				$rootScope.$watch('lastInsect', function() {
            $scope.preInit();
				});
			}],
			// controllerAs: 'vm',
		};

	};

	domReady(function () {

		/*
		 * APP MODULE
		 */
		appDirectiveBestSolution = appDirectiveBestSolution || angular.module('appDirectiveBestSolution', ['appUtils', 'appXHR', 'ngMaterial', 'ngProgress']);

		appDirectiveBestSolution
			.directive(Application.Directives);


	});

	exports.appDirectiveBestSolution = appDirectiveBestSolution;
});
