<?php
/**
 * Displays for app best-solution
 *
 * @package WordPress
 * @subpackage doom
 * @since 1.0
 * @version 1.0
 */

?>
<div class="profile-box final-container" layout="row" layout-fill layout-align="center start" layout-wrap>
	<div class="solution-container" flex="100" flex-gt-sm="50" flex-offset-gt-sm="0" style="padding: 0">
		<h3 class="page-title" style="margin:0 0 1em;">Best Solution</h3>
		<h4 class="page-title">{{result.product.post_title}}</h4>
		<span class="hr-divider"></span>
		<p class="page-description" data-ng-bind-html="result.product.post_content | trustAsHtml"></p>	
	</div>
	<div class="product-container" flex="19" flex-gt-sm="50">
		<div class="product" layout="row" layout-fill layout-align="center end">
			<img ng-src="{{result.product.image}}" alt="{{result.product.post_name}}" />
		</div>
		<div class="">
			<md-button class="primary-cta fill-width" data-ng-click="goto('/products/'+result.product.post_name)">View more</md-button>
		</div>	
	</div>
	<!-- <div class="stats-container" flex="100" flex-gt-sm="50">
		<p ng-bind-html="selectedStat.formatted"></p>
		<div class="stat-controls">
			<div class="stat-selector" ng-click="showStat(stat)" ng-repeat="stat in result.stats">
				<i class="icon icon-{{stat.type == 'molts' ? result.pest.post_name : stat.type}}" data-ng-class="{active: $first == true}"></i>
				<span>{{stat.type}}</span>
			</div>
		</div>
	</div> -->
</div>
<p>
	<small>*Please note that the insect stats and data provided would vary depending on circumstance, time, season, weather, or any other variance that could affect this information.</small>
</p>