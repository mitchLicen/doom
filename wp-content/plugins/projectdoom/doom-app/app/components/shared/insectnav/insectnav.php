<ul class="insect-insects">
    <li data-ng-click="goto('cockroach')" data-ng-class="{active: active == 'cockroach'}" class="icon-insect-cockroach" title="Cockroaches">
        <i class="icon-cockroach"></i>
    </li>
    <li data-ng-click="goto('fly')" data-ng-class="{active: active == 'fly'}" class="icon-insect-fly" title="Flies">
        <i class="icon-fly"></i>
    </li>
    <li data-ng-click="goto('mosquito')" data-ng-class="{active: active == 'mosquito'}" class="icon-insect-mosquito" title="Mosquitoes">
        <i class="icon-mosquito"></i>
    </li>
    <li data-ng-click="goto('ant')" data-ng-class="{active: active == 'ant'}" class="icon-insect-ant" title="Ants">
        <i class="icon-ant"></i>
    </li>
    <li data-ng-click="goto('fishmoth')" data-ng-class="{active: active == 'fishmoth'}" class="icon-insect-fishmoth" title="Fishmoths">
        <i class="icon-fishmoth"></i>
    </li>
    <li data-ng-click="goto('flea')" data-ng-class="{active: active == 'flea'}" class="icon-insect-flea" title="Fleas">
        <i class="icon-flea"></i>
    </li>
</ul>