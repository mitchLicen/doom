/**
* @public
*
*
* @App Dependencies [xhr, utils, memcache, routes, sessionservice, domReady]

* General-purpose Event binding. Bind any event not natively supported by Angular
* Pass an object with keynames for events to ui-event
* Allows $event object and $params object to be passed
*
* @example <input ui-event="{ focus : 'counter++', blur : 'someCallback()' }">
* @example <input ui-event="{ myCustomEvent : 'myEventHandler($event, $params)'}">
*
* @param ui-event {string|object literal} The event to bind to as a string or a hash of events with their callbacks
*
* @return Angular.module.appDirectiveInsectNav
*
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*	http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
**/
define(function (require, exports, module) {
    
      'use strict';
    
      require("app-xhr");
      require("app-utils");
      require("angular-progress");
      require("angular-material");
    
      // Load dependent modules
      var appDirectiveInsectNav,
        appConfig = JSON.parse(require("text!../../app/app.config.json")),
        domReady = require("domReady"),
        isMobile = require("isMobile"),
        angular = require("angular");
    
      //
      var Application = Application || {};
      Application.Directives = {};
    
      Application.Directives.uiInsectNav = function () {
    
        return {
          restrict: 'AE',
          transclude: false, // pass entire template?
          templateUrl: appConfig.general.path + '/app/components/shared/insectnav/insectnav.php',
          scope: {
            //attribute params
            //paramName:'@paramname',
          },
          link: function (scope, element, attrs, controller) {
            
          },
          controller: ['$rootScope', '$scope', '$sce', '$http', '$q', '$route', '$location', '$timeout', '$mdSidenav', '$log', 'transformRequestAsFormPost', 'Utils', 'ngProgress', 'pagesManager', function ($rootScope, $scope, $sce, $http, $q, $route, $location, $timeout, $mdSidenav, $log, transformRequestAsFormPost, Utils, ngProgress, pagesManager) {
            
            $scope.$on( "$routeChangeSuccess", function( ev, to, toParams, from, fromParams ){
              /**/
              _ini();

            });

            $scope.goto = function (path) {
              
              angular.element( document.querySelector('.super-hero-bg') ).removeClass('previous');
              angular.element( document.querySelector('.super-hero-bg') ).removeClass('next');
        
              $scope.scrollToTop();
              
              switch( path ) {
        
                case 'ant':
                case 'fly':
                case 'flea':
                case 'fishmoth':
                case 'mosquito':
                case 'cockroach':
        
                  $location.path('profile/' + path);
        
                  break;
        
                default:
        
                $location.path(path);
        
                  break;
        
              }
            }
            $scope.scrollToTop = function() {
              Utils.scrollWithEase(0);
            }

            $scope.$watch(function() {
              return $rootScope.lastInsect;
            }, function() {
              if ($rootScope.isConfigurator) {
                $scope.active = $rootScope.lastInsect;
              }
            }, true);

            function _ini() {
              // console.log("InsectNav Report! ",$route);
              if ($route.current.action == "products.filter.taxonomy.insecttype") {
                $scope.active = $route.current.pathParams.TYPE;
              } else {
                  $scope.active = $route.current.pathParams.ID;
                  if ($rootScope.isConfigurator && (!angular.isDefined($rootScope.lastInsect) || $rootScope.lastInsect == "")) {
                    $scope.active = 'cockroach';
                  }
              }
              
            }



            _ini();
          }],
          // controllerAs: 'vm',
        };
    
      };
    
      domReady(function () {
    
            /*
             * APP MODULE
             */
        appDirectiveInsectNav = appDirectiveInsectNav || angular.module('appDirectiveInsectNav', ['appUtils', 'appXHR', 'ngMaterial', 'ngProgress']);
    
        appDirectiveInsectNav
          .directive(Application.Directives);
    
    
      });
    
      exports.appDirectiveInsectNav = appDirectiveInsectNav;
    });
    