/**
* @public
*
*
* @App Dependencies [utils, domReady]

* General-purpose Event binding. Bind any event not natively supported by Angular
* Pass an object with keynames for events to ui-event
* Allows $event object and $params object to be passed
*
* @example <input ui-event="{ focus : 'counter++', blur : 'someCallback()' }">
* @example <input ui-event="{ myCustomEvent : 'myEventHandler($event, $params)'}">
*
* @param ui-event {string|object literal} The event to bind to as a string or a hash of events with their callbacks
*
* @return Angular.module.appDirectiveCountup
*
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*	http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
**/
define(function (require, exports, module) {

  'use strict';

  // Load dependent modules
  var appDirectiveCountup,
    appConfig = JSON.parse(require("text!../../app/app.config.json")),
    domReady = require("domReady"),
    isMobile = require("isMobile"),
    angular = require("angular");

  //
  var Application = Application || {};
  Application.Directives = {};

  Application.Directives.uiCountup = function () {

    return {
      restrict: 'AE',
      transclude: false, // pass entire template?
      template: '{{show}}',
      scope: {
        //attribute params
        //paramName:'@paramname',
        number: '@number',
        duration: '@duration'
      },
      link: function (scope, element, attrs) {

        scope.number   = typeof scope.number !== 'undefined' ? scope.number : 0;
        scope.duration = typeof scope.duration !== 'undefined' ? scope.duration : 500;
        scope.show = "0";
        scope.numberClimb(scope.number, scope.duration);
      },
      controller: ['$scope', '$timeout', function ($scope, $timeout) {
        $scope.numberClimb = function(to, duration) {
          var difference = to - $scope.show;
          var perTick = difference / duration * 10;
          return $timeout(function () {
            if (duration > 0){
              if (to.indexOf(".") > -1){
                $scope.show = (parseFloat($scope.show) + perTick).toFixed(1);
              } else {
                $scope.show = (parseFloat($scope.show) + perTick).toFixed(0);
              }
              $scope.show = parseFloat($scope.show).toString();
              if ($scope.show == to) return;
              $scope.numberClimb(to, duration - 10);
            }
          }, 10);
        }
      }],
      // controllerAs: 'vm',
    };

  };

  domReady(function () {

		/*
		 * APP MODULE
		 */
    appDirectiveCountup = appDirectiveCountup || angular.module('appDirectiveCountup', ['appUtils', 'appXHR', 'ngMaterial', 'ngProgress']);

    appDirectiveCountup
      .directive(Application.Directives);


  });

  exports.appDirectiveCountup = appDirectiveCountup;
});
