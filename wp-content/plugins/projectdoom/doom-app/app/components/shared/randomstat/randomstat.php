<?php
/**
 * Displays for app carousel
 *
 * @package WordPress
 * @subpackage doom
 * @since 1.0
 * @version 1.0
 */

?>

<div layout="column" class="randomstat">
  <div ng-if="show.length == 1">
    <p style="max-width: 100%" ng-repeat="s in show">
      <span ng-if="isRandom">
        Every {{s.time}} days, 
      </span>
      <span ng-if="!isRandom">
        Since you first visited, {{s.time}} days ago, 
      </span>
      <span ng-if="ishome">
        <strong>a single {{s.insect}}</strong> {{isRandom ? "can" : "will have"}} {{isRandom ? s.stats[randStat].firstPart : s.stats[randStat].pastFirst}} up to <span style="text-transform:uppercase;font-weight:bolder;">{{s.stats[randStat].show  | shortNumber}} {{isRandom ? s.stats[randStat].secondPart : s.stats[randStat].pastSecond}}</span> in your home.
      </span>
      <span ng-if="!ishome">
        <strong>a single {{s.insect}}</strong> can {{s.stats[randStat].firstPart}} up to<br/><span style="text-transform:uppercase;font-size: 6.625em; line-height: 1em;font-weight:bolder;">{{s.stats[randStat].show  | shortNumber}}<br/>{{s.stats[randStat].secondPart}}</span><br/>
      </span>
    </p>
  </div>
  
  <div class="" ng-if="show.length > 1">
    
    <h1>Since you first visited</h1>
    <p>{{time}} ago</p>
   
    <md-slider flex class="md-primary" md-discrete data-ng-model="dynamic.amount" step="1" min="1" max="100" aria-label="slider"></md-slider>
    
    <div ng-repeat="s in show">
      <p style="max-width: 100%;">
        <strong>{{dynamic.amount}} {{(dynamic.amount > 1) ? s.plural : s.insect}}</strong> will have {{s.stats[randStat % s.stats.length].pastFirst}} up to
        <span class="stat-counter" style="text-transform:uppercase;font-size: 1.125em; line-height: 1em;font-weight:bolder;">{{s.stats[randStat % s.stats.length].show  }}
          </span>{{s.stats[randStat % s.stats.length].pastSecond}}
        in your home.
      </p>
    </div>

  </div>
</div>
