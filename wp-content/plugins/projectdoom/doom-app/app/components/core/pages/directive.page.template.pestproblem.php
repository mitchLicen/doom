<div flex="100" flex-gt-sm="50" class="home-copy-content" <?php //flex-order="2" flex-order-gt-sm="1" ?>  layout="column" style="text-align:center;">
	
	<div class="" layout="column" style="text-align:center;">
		
		<h1 class="page-title" style="">{{pageContent.post_title}}</h1> 
		<span class="hr-divider" style="margin: 1em auto;"></span>
		<p class="page-description" data-ng-bind-html="pageContent.post_content | trustAsHtml"></p>
		
	</div>
	<?php /* * /?>
	<div class="insect-filter-categories center-content mobile-padding" style="" layout-gt-sm="row" layout-align-gt-sm="center center" >
		<md-button
			class="md-button primary-tag"
			style=""
			data-ng-click="filterInsects('flying')"
			data-ng-class="{active: pageContent.filter_type == 'flying'}">
			Flying
		</md-button>
		<md-button
			class="md-button primary-tag"
			style=""
			data-ng-click="filterInsects('crawling')"
			data-ng-class="{active: pageContent.filter_type == 'crawling'}">
			Crawling
		</md-button>
		<md-button
			class="md-button primary-tag "
			data-ng-click="filterInsects('all')"
			style=""
			data-ng-class="{active: pageContent.filter_type == 'all'}">
			All
		</md-button>
	</div> 
	<?php /**/?>
	<div class="center-content" style="width:100%;"  layout-gt-sm="row" hide-xs hide-sm  layout-align-gt-sm="center center" style="margin:40px auto;text-align:center;">
		<ul class="product-insects home-activity" insect-type="all"> 
			<li data-ng-class="{active: pageContent.insect_cockroach == true, stat: activeInsect.post_name == 'cockroach'}" class="icon-insect-cockroach" title="Cockroaches">
				<button type="button" data-ng-disabled="!pageContent.insect_cockroach" class="md-btn" data-ng-click="whatsBuggin('cockroach', false, true)">
					<i class="icon-cockroach"></i>
					<p>Cockroaches</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_fly == true, stat: activeInsect.post_name == 'fly'}" class="icon-insect-fly" title="Flies">
				<button type="button" data-ng-disabled="!pageContent.insect_fly" class="md-btn" data-ng-click="whatsBuggin('fly', false, true)">	
					<i class="icon-fly"></i>
					<p>flies</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_mosquito == true, stat: activeInsect.post_name == 'mosquito'}" class="icon-insect-mosquito" title="Mosquitoes">
				<button  data-ng-disabled="!pageContent.insect_mosquito" type="button" class="md-btn"  data-ng-click="whatsBuggin('mosquito', false, true)">
					<i class="icon-mosquito"></i>
					<p>Mosquitoes</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_ant == true, stat: activeInsect.post_name == 'ant' }" class="icon-insect-ant" title="Ants">
				<button type="button" data-ng-disabled="!pageContent.insect_ant" class="md-btn" data-ng-click="whatsBuggin('ant', false, true)">
					<i class="icon-ant"></i>
					<p>Ants</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_fishmoth == true, stat: activeInsect.post_name == 'fishmoth'}" class="icon-insect-fishmoth" title="Fishmoths">
				<button type="button" data-ng-disabled="!pageContent.insect_fishmoth" class="md-btn" data-ng-click="whatsBuggin('fishmoth', false, true)">
					<i class="icon-fishmoth"></i>
					<p>fishmoths</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_flea == true, stat: activeInsect.post_name == 'flea'}" class="icon-insect-flea" title="Fleas">
				<button type="button" data-ng-disabled="!pageContent.insect_flea" class="md-btn" data-ng-click="whatsBuggin('flea', false, true)">
					<i class="icon-flea"></i>
					<p>fleas</p>
				</button>
			</li>
		</ul>
	</div>
	<!-- <div class="" layout="column">
		
		<div layout-gt-sm="row" layout-fill layout-align="center center" style="margin:50px auto;margin-bottom:0;">

			<ui-randomstat id="home-page-stat" ishome="true" active-insect="{{activeInsect.post_name}}" duration="1500"></ui-randomstat>
			<ui-randomstat  layout="column" flex="100" timer="true"></ui-randomstat>

		</div>
		

	</div> -->
	<div class="super-hero-fact-holder out" hide show-gt-sm>
		<div data-ng-switch="activeInsect.post_name">
			<div data-ng-switch-when="flea" class="insect-fact">
				<!-- <h1 flex="100" class="page-title main"><br/>FLEAS</h1> -->
				<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
				<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}s</strong> can suck</p>
				<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}--><ui-countup number='15' duration="500"></ui-countup> times<sup>*</sup></h1>
				<p flex="100" class="page-description">their bodyweight in blood a day. <?php //Their fave drinking spot is covered in fur and answers to the name, Scruffles.?></p>
			</div>
			<div data-ng-switch-when="cockroach" class="insect-fact">
				<!-- <h1 flex="100" class="page-title main">Cock<br/>roaches</h1> -->
				<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
				<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}es</strong> can go</p>
				<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}><br /--><ui-countup number='7' duration="500"></ui-countup>  days<sup>*</sup></h1>
				<p flex="100" class="page-description">without a head. <?php //That means a headless hedonistic roach could be making its way up your PJs while you sleep tonight.?></p>
			</div>
			<div data-ng-switch-when="ant" class="insect-fact">
				<!-- <h1 flex="100" class="page-title main"><br/>Ants</h1> -->
				<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
				<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}s</strong> vomit every</p>
				<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}<br /-->second<sup>*</sup></h1>
				<p flex="100" style="max-width:90%;" class="page-description"><?php //That includes the ones currently hijacking the sandwich in your skaftin.?> </p>
			</div>
			<div data-ng-switch-when="fly" class="insect-fact"> 
				<!-- <h1 flex="100" class="page-title main"><br/>Flies</h1> -->
				<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
				<p flex="100" class="page-description"><strong><!--{{activeInsect.post_title}}-->Flies</strong> poop once every </p>
				<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}<br /--><ui-countup number='10' duration="500"></ui-countup>  seconds<sup>*</sup></h1>
				<p flex="100" class="page-description">while feeding. <?php //Good to know for next time you spot a fly snacking on your Sunday seven colours.?></p>
			</div>
			<div data-ng-switch-when="mosquito" class="insect-fact">
				<!-- <h1 flex="100" class="page-title main">Mos<br/>quitoes</h1> -->
				<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
				<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}es</strong> can lay up to </p>
				<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}<br /--><ui-countup number='150' duration="500"></ui-countup>  eggs<sup>*</sup></h1>
				<p flex="100" class="page-description">at a time.<?php // Only females bite, and they’re particularly fond of pungent odours, like your smelly feet. ?></p>
			</div> 
			<div data-ng-switch-when="fishmoth" class="insect-fact">
				<!-- <h1 flex="100" class="page-title main">Fish<br/>moths</h1> -->
				<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
				<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}s</strong> <?php //eating holes through the private bits of your underwear?> are laying</p>
				<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}<br /--><ui-countup number='2.5' duration="500"></ui-countup> eggs<sup>*</sup></h1>
				<p flex="100" class="page-description">a day, in your home.</p>
			</div>
			
		</div>
	</div>
	<div class="main-cta-container" layout="column" hide-xs hide-sm style="" layout-align="center center">
		
		<p class="page-description" style="font-size: 12px;">Had enough? Hit Make Them Stop below to get the right solution for you.</p>
		<!-- <md-button ng-click="goto(activeInsect.post_name)" class="md-primary primary-cta" style="padding:10px 45px;font-size:30px;margin: 44px 0;width:300px;">start</md-button> -->
		<md-button ng-click="goto('/makethemstop')" class="md-primary primary-cta main">Make Them Stop</md-button>

	</div>

</div>
<div class="home-image" flex="100" flex-offset-gt-sm="10" flex-gt-sm="40" <?php //flex-order="1" flex-order-gt-sm="2" ?> class="" style="" layout-align="center center">
	<div id="mob-accordion" class="mob-accordion" data-ng-class="{in: !isHome}">
		
		<div class="super-bug-image-holder" hide show-gt-sm data-ng-mouseover="viewFact()">
			<img ng-animate-swap="activeInsect" data-ng-src="{{activeInsect.image}}" data-ng-click="" flex="100" class="super-bug-image {{activeInsect.post_name}}" alt="" />
		</div>
		<div class="super-bug-image-holder mobile" hide-gt-sm>
			<img ng-animate-swap="activeInsect" data-ng-src="{{activeInsect.side_image}}" data-ng-click="" flex="100" class="super-bug-image {{activeInsect.post_name}}" alt="" />
			<div class="insect-controller-buttons" hide-gt-sm>
				<button class="icon-chevron-left-thin" data-nav-direction="back" data-ng-click="whatsBugginPrev()"></button>
				<button class="icon-chevron-right-thin" data-nav-direction="forward" data-ng-click="whatsBugginNext()"></button>
			</div>
		</div>
		<div class="super-hero-fact-holder out" hide-gt-sm>
			<div data-ng-switch="activeInsect.post_name">
				<div data-ng-switch-when="flea" class="insect-fact">
					<!-- <h1 flex="100" class="page-title main"><br/>FLEAS</h1> -->
					<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
					<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}s</strong> can suck</p>
					<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}--><ui-countup number='15' duration="500"></ui-countup> times<sup>*</sup></h1>
					<p flex="100" class="page-description">their bodyweight in blood a day. <?php //Their fave drinking spot is covered in fur and answers to the name, Scruffles.?></p>
				</div>
				<div data-ng-switch-when="cockroach" class="insect-fact">
					<!-- <h1 flex="100" class="page-title main">Cock<br/>roaches</h1> -->
					<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
					<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}es</strong> can go</p>
					<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}><br /--><ui-countup number='7' duration="500"></ui-countup>  days<sup>*</sup></h1>
					<p flex="100" class="page-description">without a head. <?php //That means a headless hedonistic roach could be making its way up your PJs while you sleep tonight.?></p>
				</div>
				<div data-ng-switch-when="ant" class="insect-fact">
					<!-- <h1 flex="100" class="page-title main"><br/>Ants</h1> -->
					<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
					<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}s</strong> vomit every</p>
					<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}<br /-->second<sup>*</sup></h1>
					<p flex="100" style="max-width:90%;" class="page-description"><?php //That includes the ones currently hijacking the sandwich in your skaftin.?> </p>
				</div>
				<div data-ng-switch-when="fly" class="insect-fact"> 
					<!-- <h1 flex="100" class="page-title main"><br/>Flies</h1> -->
					<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
					<p flex="100" class="page-description"><strong><!--{{activeInsect.post_title}}-->Flies</strong> poop once every </p>
					<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}<br /--><ui-countup number='10' duration="500"></ui-countup>  seconds<sup>*</sup></h1>
					<p flex="100" class="page-description">while feeding. <?php //Good to know for next time you spot a fly snacking on your Sunday seven colours.?></p>
				</div>
				<div data-ng-switch-when="mosquito" class="insect-fact">
					<!-- <h1 flex="100" class="page-title main">Mos<br/>quitoes</h1> -->
					<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
					<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}es</strong> can lay up to </p>
					<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}<br /--><ui-countup number='150' duration="500"></ui-countup>  eggs<sup>*</sup></h1>
					<p flex="100" class="page-description">at a time.<?php // Only females bite, and they’re particularly fond of pungent odours, like your smelly feet. ?></p>
				</div> 
				<div data-ng-switch-when="fishmoth" class="insect-fact">
					<!-- <h1 flex="100" class="page-title main">Fish<br/>moths</h1> -->
					<!-- <span class="hr-divider" style="margin: 1em 0; width: 96px;"></span> -->
					<p flex="100" class="page-description"><strong>{{activeInsect.post_title}}s</strong> <?php //eating holes through the private bits of your underwear?> are laying</p>
					<h1 flex="100" class="page-title stat" style="color:#000;"><!--{{activeInsect.doom_insect_egg}}<br /--><ui-countup number='2.5' duration="500"></ui-countup> eggs<sup>*</sup></h1>
					<p flex="100" class="page-description">a day, in your home.</p>
				</div>
				
			</div>
		</div>
		<div ng-animate-swap="activeInsect" class="insect-buttons-cta desktop" hide-gt-sm layout="row" layout-align="center start" flex="100" layout-gt-sm="row" layout-align-gt-sm="center start">
			<div>
				<md-button class="md-primary primary-tag" data-ng-click="viewProfile(activeInsect.post_name)">
					<span class="icon "><i class="icon-{{activeInsect.post_name}}"></i></span>
					{{activeInsect.post_title}}<br />Profile
				</md-button>
			</div>
			<div>
				<md-button class="md-primary primary-tag" data-ng-click="browseBycategoryInsect( 'products', activeInsect.insect_categories[0].slug, activeInsect.post_name)">
					<span class="icon icon-"><i class="icon-zap"></i></span>
					{{activeInsect.post_title}}<br />Solutions
				</md-button>
			</div>
		</div>

		<div class="description-holder" data-ng-mouseleave="hideFact()">
			<div class="heading-holder mobile">
				<div ng-animate-swap="activeInsect" data-ng-switch="activeInsect.post_name" class="">
					<h3 flex="100" class="page-title" style="" data-ng-switch-when="cockroach">The Serial Cuddler</h3>
					<h3 flex="100" class="page-title" style="" data-ng-switch-when="fly">The Restless Romantic</h3>
					<h3 flex="100" class="page-title" style="" data-ng-switch-when="fishmoth">Strict Diet</h3>
					<h3 flex="100" class="page-title" style="" data-ng-switch-when="mosquito">The Itching Truth</h3>
					<h3 flex="100" class="page-title" style="" data-ng-switch-when="ant">The Ant Factor</h3>
					<h3 flex="100" class="page-title" style="" data-ng-switch-when="flea">Jumper</h3>
				</div>
			</div>
			<p ng-animate-swap="activeInsect" flex="100" class="page-description" data-ng-bind-html="activeInsect.post_content | trustAsHtml">{{activeInsect.post_content}}</p>

			<div class="cta-holder" hide show-gt-sm>
				<div ng-animate-swap="activeInsect" class="insect-buttons-cta" layout="row" layout-align="center start" flex="100" layout-gt-sm="row" layout-align-gt-sm="center start">
					<div>
						<md-button class="md-primary primary-tag" data-ng-click="viewProfile(activeInsect.post_name)">
							<span class="icon "><i class="icon-{{activeInsect.post_name}}"></i></span>
							{{activeInsect.post_title}}<br />Profile
						</md-button>
					</div>
					<div>
						<md-button class="md-primary primary-tag" data-ng-click="browseBycategoryInsect( 'products', activeInsect.insect_categories[0].slug, activeInsect.post_name)">
							<span class="icon icon-"><i class="icon-zap"></i></span>
							{{activeInsect.post_title}}<br />Solutions
						</md-button>
					</div>
				</div>
				<!-- <div class="insect-controller-buttons" hide show-gt-sm>
					<button class="icon-chevron-left-thin" data-nav-direction="back" data-ng-click="whatsBugginPrev()"></button>
					<button class="icon-chevron-right-thin" data-nav-direction="forward" data-ng-click="whatsBugginNext()"></button>
				</div> -->
			</div>

		</div>
		<!-- <div class="cta-holder mobile hovering" hide show-gt-sm>
			<div ng-animate-swap="activeInsect" class="insect-buttons-cta" layout="row" layout-align="center start" flex="100" layout-gt-sm="column" layout-align-gt-sm="center start">
				<div>
					<md-button class="md-primary primary-tag" data-ng-click="viewFact()">
						<span class="icon "><i class="icon-star"></i></span>
						{{activeInsect.post_title}}<br />Facts
					</md-button>
				</div>
				<div>
					<md-button class="md-primary primary-tag" data-ng-click="viewProfile(activeInsect.post_name)">
						<span class="icon "><i class="icon-{{activeInsect.post_name}}"></i></span>
						{{activeInsect.post_title}}<br />Profile
					</md-button>
				</div>
				<div>
					<md-button class="md-primary primary-tag" data-ng-click="browseBycategoryInsect( 'products', activeInsect.insect_categories[0].slug, activeInsect.post_name)">
						<span class="icon icon-"><i class="icon-zap"></i></span>
						{{activeInsect.post_title}}<br />Solutions
					</md-button>
				</div>
			</div>
			<div class="insect-controller-buttons" hide show-gt-sm>
				<button class="icon-chevron-left-thin" data-nav-direction="back" data-ng-click="whatsBugginPrev()"></button>
				<button class="icon-chevron-right-thin" data-nav-direction="forward" data-ng-click="whatsBugginNext()"></button>
			</div>
		</div> -->
		<div class="main-cta-container" layout="column" hide-gt-sm style="" layout-align="center center">
			
			<p class="page-description">Had enough? Hit Make Them Stop below to get the right {{pageContent.post_title}} Solution for you.</p>
			<!-- <md-button ng-click="goto(activeInsect.post_name)" class="md-primary primary-cta" style="padding:10px 45px;font-size:30px;margin: 44px 0;width:300px;">start</md-button> -->
			<md-button ng-click="goto('/makethemstop')" class="md-primary primary-cta main">Make Them Stop</md-button>

		</div>
	</div>
	<div class="center-content" style="width:100%;overflow: hidden;"  layout-gt-sm="row" hide-gt-sm layout-align-gt-sm="center center" style="margin:40px auto;text-align:center;">
		<ul class="product-insects home-activity" insect-type="all"> 
			<li data-ng-class="{active: pageContent.insect_cockroach == true, stat: activeInsect.post_name == 'cockroach'}" class="icon-insect-cockroach" title="Cockroaches">
				<button type="button" data-ng-disabled="!pageContent.insect_cockroach" class="md-btn" data-ng-click="whatsBuggin('cockroach', false, true)">
					<i class="icon-cockroach"></i>
					<br />
					<p>Cockroaches</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_fly == true, stat: activeInsect.post_name == 'fly'}" class="icon-insect-fly" title="Flies">
				<button type="button" data-ng-disabled="!pageContent.insect_fly" class="md-btn" data-ng-click="whatsBuggin('fly', false, true)">	
					<i class="icon-fly"></i>
					<br />
					<p>flies</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_mosquito == true, stat: activeInsect.post_name == 'mosquito'}" class="icon-insect-mosquito" title="Mosquitoes">
				<button  data-ng-disabled="!pageContent.insect_mosquito" type="button" class="md-btn"  data-ng-click="whatsBuggin('mosquito', false, true)">
					<i class="icon-mosquito"></i>
					<br />
					<p>Mosquitoes</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_ant == true, stat: activeInsect.post_name == 'ant' }" class="icon-insect-ant" title="Ants">
				<button type="button" data-ng-disabled="!pageContent.insect_ant" class="md-btn" data-ng-click="whatsBuggin('ant', false, true)">
					<i class="icon-ant"></i>
					<br />
					<p>Ants</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_fishmoth == true, stat: activeInsect.post_name == 'fishmoth'}" class="icon-insect-fishmoth" title="Fishmoths">
				<button type="button" data-ng-disabled="!pageContent.insect_fishmoth" class="md-btn" data-ng-click="whatsBuggin('fishmoth', false, true)">
					<i class="icon-fishmoth"></i>
					<br />
					<p>fishmoths</p>
				</button>
			</li>
			<li data-ng-class="{active: pageContent.insect_flea == true, stat: activeInsect.post_name == 'flea'}" class="icon-insect-flea" title="Fleas">
				<button type="button" data-ng-disabled="!pageContent.insect_flea" class="md-btn" data-ng-click="whatsBuggin('flea', false, true)">
					<i class="icon-flea"></i>
					<br />
					<p>fleas</p>
				</button>
			</li>
		</ul>
	</div>
</div>