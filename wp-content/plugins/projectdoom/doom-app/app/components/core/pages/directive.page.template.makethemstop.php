<div layout="column" layout-fill class="animate-page-switch-container" flex="100" data-ng-switch="renderPath[1]">

    <!-- CONFIGURATOR -->
    <div layout="column" layout-align-gt-sm="start start" layout-align="center center">
        <!-- <br /><br /> -->
        <h1 id="makethemstop" class="page-title" style="margin-top:0px;text-align:left;font-size:35pt;">Make Them Stop</h1>
        <p class="page-description" style="margin-bottom:0;">Get the right {{pageContent.post_name}} Solution for your {{pageContent.post_name}} problem.</p>
        <!-- <br /><br /> -->
    </div>
    <div layout-gt-sm="row" layout="column" layout-align="center center">

        <div data-ui-configurator style="" pest="{{lastInsect}}" flex="100"></div>

    </div>

</div>