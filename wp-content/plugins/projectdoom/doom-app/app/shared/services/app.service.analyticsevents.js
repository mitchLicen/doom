/**
* @private
* 
* 
* @App Dependencies [ app-utils, domReady ]
*
* @return Angular.appAnalyticsEvents
* 
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*	http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
**/
define( function ( require, exports, module ) {
	
    'use strict';
    
    require("app-utils");

	// Load dependent modules
    var appAnalyticsEvents,
		domReady 		= require("domReady"),
		angular 		= require("angular");

	// 
	var Application = Application || {};
	Application.Services = {};
	
	/*
	 * 
	 * ------------------------------------------------*/
	Application.Services.AnalyticsEvents = [ '$q', 'Utils', function ( $q, Utils ) {

		var factory = this;

        this.send = function (category, action, label, value) {
            if( angular.isDefined(window.ga) ) {
                
                console.log( 'Sending Google analytics event: ', category, action, label, value);

                ga(
                    "send",
                    "event",
                    category,
                    action,
                    label,
                    value
                );
    

            } else {

                console.warn( 'Google analytics not initialised! event: ', category, action, label, value);
            }
        }
        this.sendPage = function ( action ) {
            if( angular.isDefined(window.ga) ) {
                
                console.log( 'Sending Google analytics PageView event: ', action);

                ga(
                    "send",
                    "pageview",
                    action
                );
    

            } else {

                console.warn( 'Google analytics not initialised! PageView event: ', action);
            }
        }

		/**
	     * 
	     *
	     * @param String
	     * 
	     * @return Object
	     */

		return factory;

	}]

	domReady( function () {

		/*
		 * APP MODULE
		 */
		appAnalyticsEvents = appAnalyticsEvents || angular.module( 'appAnalyticsEvents', [ 'appUtils' ] );

		appAnalyticsEvents
			.service( Application.Services );


	});
		
	exports.appAnalyticsEvents = appAnalyticsEvents;
});