// $(document).ready(function(){
//     // PointerEventsPolyfill.initialize({});
    
//     // window.pointerEventsPolyfill();
// });

// $(function(){
//     // window.pointerEventsPolyfill();
// });

// define( function ( require, exports, module ) {
    
//     'use strict';
//     var domReady 		= require("domReady");
    
//     domReady( function () {
//     //    console.log(window);
//     });
// });

window.route = {
    pageLoaded: false,
    aListener: function(val) {},
    set loaded(val) {
        this.pageLoaded = val;
        this.aListener(val);
    },
    get loaded() {
        return this;
    },
    registerListener: function(listener) {
        this.aListener = listener;
    }
};

window.route.registerListener(function(val) {
    console.log("Window loaded?", val);

    if (val) {
        refresh();
    }
});

var refresh = function() {
    document.querySelector(".animate-page-switch .layout-gt-sm-row")
    var arr = [
        ".animate-page-switch .layout-gt-sm-row"
    ];
    setTimeout(function(){
        console.log("Refresh layout");
        var elems = document.querySelector(arr.join(','));
        if (elems) {
            elems.style.height = 0;
            elems.style.minHeight = 0;
            elems.style.display = "none";

            setTimeout(function(){
                elems.style.height = "";
                elems.style.minHeight = "";
                elems.style.display = "";
    
                console.log("Reset layout");
            }, 10);
        }
    }, 500);
}