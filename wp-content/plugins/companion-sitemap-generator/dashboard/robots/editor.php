<div class="wrap">

	<div class="csg-column-wide">

		<h1><?php _e('Robots', 'companion-sitemap-generator'); ?></h1>

		<?php if( csg_has_robots() ) {

			// Read robots.txt file
			function csg_read_robots() {

				$csg_websiteRoot 	= get_home_path();
				$csg_robotsFile		= $csg_websiteRoot.'/robots.txt';
				$robotLines 		= array();
				$readRobots 		= fopen($csg_robotsFile, "r");

				if ($readRobots) {
					while (($lineRobot = fgets($readRobots)) !== false) {
					    array_push( $robotLines, $lineRobot );
					}
					fclose($readRobots);
				}

				foreach ($robotLines as $robotLine) { 
					echo $robotLine; echo ''; 
				}
			}

			// Write to the robots.txt file
			function csg_write_robots( $contentToWrite = '' ) {

				$csg_websiteRoot = get_home_path();
				$csg_robotsFile = $csg_websiteRoot.'/robots.txt';

				$filename = $csg_robotsFile;

				if (is_writable($filename)) {

				    if (!$handle = fopen($filename, 'w')) {
				         errorMSG("Cannot open file robots.txt");
				         exit;
				    }

				    if (fwrite($handle, $contentToWrite) === FALSE) {
				        errorMSG("Something went wrong.");
				        exit;
				    }

				    succesMSG( __( 'Your robots file has been updated succesfully. Be sure to reload the page before making further adjustments.', 'companion-sitemap-generator' ) );

				    fclose($handle);

				} else {
				    errorMSG( __( 'The file robots.txt is not writable', 'companion-sitemap-generator' ) );
				}
			}

			if( isset( $_POST['csg_saveRobots'] ) ) {
				check_admin_referer( 'csg_save_robots' );
				csg_write_robots( sanitize_textarea_field( $_POST['csg_robots_content'] ) ); 
			}

			?>

			<table class="form-table">
				
				<tr>
					<th scope="row">
						<?php _e('Edit Robots', 'companion-sitemap-generator');?>				
						<p style="font-weight: normal;"><?php _e('While a sitemap allows search engines to scan pages faster, a robots.txt file disallows search engines from scanning certain pages.', 'companion-sitemap-generator');?></p>
					</th>
					<td>
						<form method="POST">
							<textarea name="csg_robots_content" rows="12" cols="45"><?php csg_read_robots(); ?></textarea>
							<?php wp_nonce_field( 'csg_save_robots' ); ?>	
							<p><button type='submit' name='csg_saveRobots' class='button button-primary'><?php _e('Save robots', 'companion-sitemap-generator');?></button></p>
						</form>
					</td>
				</tr>

				<tr>
					<th scope="row"><?php _e('Sitemap & Robots', 'companion-sitemap-generator');?></th>
					<td>
						<p><?php _e('Use this line if you\'d like to add the sitemap link to the robots file (good for SEO):', 'companion-sitemap-generator');?></p>
						<p><code>Sitemap: <?php echo csg_sitemap_url(); ?></code></p>
					</td>
				</tr>

				<tr>
					<th scope="row">
						<?php _e('Basic Example', 'companion-sitemap-generator');?>
						<p style='font-weight: normal;'><?php _e('Here\'s an example of what a robots file could look like.', 'companion-sitemap-generator');?></p>
					</th>
					<td>

						User-agent: *<br />
						Disallow: /wp-admin/<br />
						Disallow: /wp-includes/<br />
						Disallow: /feed/<br />
						Disallow: */feed/<br /><br />
						
						<p><a href='https://support.google.com/webmasters/answer/6062608' rel='nofollow' target='_blank' class='button'><?php _e('Read more about robots', 'companion-sitemap-generator');?></a></p>
					</td>
				</tr>
			</table>

		<?php } else { 

			if( isset( $_POST['csg_createRobots'] ) ) {
				check_admin_referer( 'csg_create_robots' );
				csg_create_robots();
				header( "Location: ".admin_url( 'tools.php?page=csg-robots' ) ); 
			}

			?>

			<p>
				<?php _e('In order to use the robots editor you\'ll first need to create a robots file.', 'companion-sitemap-generator'); ?>
				<?php _e('This will create a physical robots.txt file in your root folder. ', 'companion-sitemap-generator'); ?>
			</p>

			<form method="POST">
				<?php wp_nonce_field( 'csg_create_robots' ); ?>	
				<p><button type='submit' name='csg_createRobots' class='button button-primary'><?php _e('Create file', 'companion-sitemap-generator');?></button></p>
			</form>

		<?php } ?>

	</div><div class="csg-column-small">

		<div class="welcome-to-csg love-bg csg-show-love csg-dashboard-box">
			<h3><?php _e( 'Like our plugin?', 'companion-sitemap-generator' ); ?></h3>
			<p><?php _e('Companion Sitemap Generator is free to use. It has required a great deal of time and effort to develop and you can help support this development by making a small donation.<br />You get useful software and we get to carry on making it better.', 'companion-sitemap-generator'); ?></p>
			<a href="https://wordpress.org/support/plugin/companion-sitemap-generator/reviews/#new-post" target="_blank" class="csg-button rate-button">
				<span class="dashicons dashicons-star-filled"></span> 
				<?php _e('Rate us (5 stars?)', 'companion-sitemap-generator'); ?>
			</a>
			<a href="<?php echo csg_donateUrl(); ?>" target="_blank" class="csg-button donate-button">
				<span class="dashicons dashicons-heart"></span> 
				<?php _e('Donate to help development', 'companion-sitemap-generator'); ?>
			</a>
			<p style="font-size: 12px; color: #BDBDBD;">Donations via PayPal. Amount can be changed.</p>
		</div>

	</div>

</div>