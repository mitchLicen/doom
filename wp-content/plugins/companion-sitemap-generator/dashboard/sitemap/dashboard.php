<?php
		
if( isset( $_POST['csg_generate'] ) ) {
	csg_sitemap();
}

?>


	<div class="welcome-to-csg csg-dashboard-box">
		
		<div class="welcome-column welcome-column welcome-column-third">

			<h3><?php _e( 'Sitemap link' ); ?></h3>
			<p><?php echo csg_sitemap_url(); ?></p>
			<p><a href='<?php echo csg_sitemap_url(); ?>' target='_blank' class='button button-primary'><?php _e('View sitemap', 'companion-sitemap-generator'); ?></a></p>
			
		</div><div class="welcome-column welcome-column welcome-column-third">

			<h3><?php _e( 'Update Sitemap', 'companion-sitemap-generator' ); ?></h3>
			<p><?php _e( 'We update it every hour, but you can update it yourself.', 'companion-sitemap-generator');?></p>
			<form method="POST">
				<p><button type="submit" name="csg_generate" class="button button-alt"><?php echo csg_dynamic_button(); ?></button></a></p>
			</form>

		</div><div class="welcome-column welcome-column welcome-column-third">

			<h3><?php _e( 'Robots' ); ?></h3>
			<p><?php _e( 'Looking for the robots editor?', 'companion-sitemap-generator');?></p>
			<p><a href='<?php echo admin_url( 'tools.php?page=csg-robots' ); ?>' target='_blank' class='button button-alt'><?php _e( 'Edit Robots', 'companion-sitemap-generator' ); ?></a></p>
			
		</div>

	</div>

	<div class="welcome-to-csg gutenberg-bg">
		<div class="welcome-column welcome-column-first welcome-column">
			<h3><?php _e( 'Want to add the HTML sitemap to your page?', 'companion-sitemap-generator' ); ?></h3>
			<p><?php _e( "In version 4 we've removed the shortcode generator in favor of the new fancy gutenberg block. Just add the HTML sitemap block to your page.", "companion-sitemap-generator" ); ?></p>
			<p><?php _e( "Don't care for gutenberg?", "companion-sitemap-generator" ); ?> 
			<a href='https://codeermeneer.nl/documentation/how-to-use-the-shortcode-in-companion-sitemap-generator/' target='_blank'><?php _e( "You can still use the shortcode", "companion-sitemap-generator" ); ?>. </a></p>
		</div>
	</div>

	<div class="welcome-to-csg love-bg csg-show-love">
		<div class="welcome-column welcome-column-first welcome-column welcome-column-half">
			<h3><?php _e( 'Like our plugin?', 'companion-sitemap-generator' ); ?></h3>
			<p><?php _e('Companion Sitemap Generator is free to use. It has required a great deal of time and effort to develop and you can help support this development by making a small donation.<br />You get useful software and we get to carry on making it better.', 'companion-sitemap-generator'); ?></p>
		</div><div class="welcome-column welcome-column welcome-column-half">
			<a href="https://wordpress.org/support/plugin/companion-sitemap-generator/reviews/#new-post" target="_blank" class="csg-button rate-button">
				<span class="dashicons dashicons-star-filled"></span> 
				<?php _e('Rate us (5 stars?)', 'companion-sitemap-generator'); ?>
			</a><a href="<?php echo csg_donateUrl(); ?>" target="_blank" class="csg-button donate-button">
				<span class="dashicons dashicons-heart"></span> 
				<?php _e('Donate to help development', 'companion-sitemap-generator'); ?>
			</a>
			<p style="font-size: 12px; color: #BDBDBD;">Donations via PayPal. Amount can be changed.</p>
		</div>
	</div>

</div>
