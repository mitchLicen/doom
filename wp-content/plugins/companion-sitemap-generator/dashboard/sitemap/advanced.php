<?php

$sitemap_schedule 	= wp_get_schedule( 'csg_create_sitemap' );
		
if( isset( $_POST['csg_generate'] ) ) {
	csg_sitemap();
}

if( isset( $_POST['submit'] ) ) {

	check_admin_referer( 'csg_save_advanced' );

	// Save frequency
	global $wpdb;
	$table_name = $wpdb->prefix . "sitemap"; 

	if( isset( $_POST["frequency"] ) ) $changeFreq = sanitize_text_field( $_POST['frequency'] ); else $changeFreq = '';
	if( isset( $_POST["use_sitemap_stylesheet"] ) ) $use_sitemap_stylesheet = $_POST["use_sitemap_stylesheet"]; else $use_sitemap_stylesheet = '';
	if( isset( $_POST["sitemap_stylesheet"] ) ) $sitemap_stylesheet = sanitize_text_field( $_POST['sitemap_stylesheet'] ); else $sitemap_stylesheet = '';

	$wpdb->query( $wpdb->prepare( "UPDATE $table_name SET onoroff = %s WHERE name = 'frequency'", $changeFreq ) );
	$wpdb->query( $wpdb->prepare( "UPDATE $table_name SET onoroff = %s WHERE name = 'use_sitemap_stylesheet'", $use_sitemap_stylesheet ) );
	$wpdb->query( $wpdb->prepare( "UPDATE $table_name SET onoroff = %s WHERE name = 'sitemap_stylesheet'", $sitemap_stylesheet ) );

	// Save schedule
	// Set variables
	$sitemap_sc = sanitize_text_field( $_POST['sitemap_schedule'] );

	// First clear schedule
	wp_clear_scheduled_hook('csg_create_sitemap');

	// Then set the new times
	wp_schedule_event( time(), $sitemap_sc, 'csg_create_sitemap' );

	echo '<div id="message" class="updated"><p>'.__('Settings saved', 'companion-sitemap-generator').'.</p></div>';

}

?>

<div class="csg-column-wide">

	<form method="POST">

		<table class="form-table">
			
			<tr>
				<th scope="row"><?php _e('Frequency', 'companion-sitemap-generator');?></th>
				<td>
					<select name='frequency'>
						<option <?php if( changeFreq() == 'always' ) { echo 'SELECTED'; } ?> value="always"><?php _e('Always', 'companion-sitemap-generator');?></option>
						<option <?php if( changeFreq() == 'hourly' ) { echo 'SELECTED'; } ?> value="hourly"><?php _e('Hourly', 'companion-sitemap-generator');?></option>
						<option <?php if( changeFreq() == 'daily' ) { echo 'SELECTED'; } ?> value="daily"><?php _e('Daily', 'companion-sitemap-generator');?></option>
						<option <?php if( changeFreq() == 'weekly' ) { echo 'SELECTED'; } ?> value="weekly"><?php _e('Weekly', 'companion-sitemap-generator');?></option>
						<option <?php if( changeFreq() == 'monthly' ) { echo 'SELECTED'; } ?> value="monthly"><?php _e('Monthly', 'companion-sitemap-generator');?></option>
						<option <?php if( changeFreq() == 'yearly' ) { echo 'SELECTED'; } ?> value="yearly"><?php _e('Yearly', 'companion-sitemap-generator');?></option>
						<option <?php if( changeFreq() == 'never' ) { echo 'SELECTED'; } ?> value="never"><?php _e('Never', 'companion-sitemap-generator');?></option>
					</select>
					<p class="description"><?php _e('How frequently the page is likely to change. This value provides general information to search engines and may not correlate exactly to how often they crawl the page.', 'companion-sitemap-generator');?></p>
				</td>
			</tr>

		</table>

		<h2 class="title"><?php _e('Auto updating', 'companion-sitemap-generator');?></h2>
		<table class="form-table">
			<tr>
				<th scope="row"><?php _e('Auto updating', 'companion-sitemap-generator');?></th>
				<td>
					<p>
						<select name='sitemap_schedule' id='sitemap_schedule'>
							<option value='hourly' <?php if( $sitemap_schedule == 'hourly' ) { echo "SELECTED"; } ?> ><?php _e('Hourly', 'companion-sitemap-generator');?></option>
							<option value='twicedaily' <?php if( $sitemap_schedule == 'twicedaily' ) { echo "SELECTED"; } ?> ><?php _e('Twice Daily', 'companion-sitemap-generator');?></option>
							<option value='daily' <?php if( $sitemap_schedule == 'daily' ) { echo "SELECTED"; } ?> ><?php _e('Daily', 'companion-sitemap-generator');?></option>
						</select>
					</p>
					<p class="description"><?php _e('How often should the sitemap update? (Default hourly)', 'companion-sitemap-generator'); ?></p>
				</td>
			</tr>
		</table>

		<h2 class="title"><?php _e('Advanced', 'companion-sitemap-generator');?></h2>
		<table class="form-table">
			<tr>
				<th scope="row"><?php _e( 'Use sitemap stylesheet?', 'companion-sitemap-generator' );?></th>
				<td>
					<p>
						<input type="checkbox" name="use_sitemap_stylesheet" id="use_sitemap_stylesheet" <?php if( csg_use_XMLstylesheet() ) { echo "CHECKED"; } ?>>
					</p>
				</td>
			</tr>
			<tr>
				<th scope="row"><?php _e( 'Sitemap stylesheet', 'companion-sitemap-generator' );?></th>
				<td>
					<p>
						<input type="text" name="sitemap_stylesheet" id="sitemap_stylesheet" class="regular-text" value="<?php echo csg_XMLstylesheet(); ?>">
					</p>
					<p class="description"><?php _e( 'Change the stylesheet for the XML sitemap, use full URL', 'companion-sitemap-generator' ); ?></p>
				</td>
			</tr>
		</table>

		<?php wp_nonce_field( 'csg_save_advanced' ); ?>	

		<?php submit_button(); ?>

	</form>

</div><div class="csg-column-small">
	
	<div class="welcome-to-csg love-bg csg-show-love csg-dashboard-box">
		<h3><?php _e( 'Like our plugin?', 'companion-sitemap-generator' ); ?></h3>
		<p><?php _e('Companion Sitemap Generator is free to use. It has required a great deal of time and effort to develop and you can help support this development by making a small donation.<br />You get useful software and we get to carry on making it better.', 'companion-sitemap-generator'); ?></p>
		<a href="https://wordpress.org/support/plugin/companion-sitemap-generator/reviews/#new-post" target="_blank" class="csg-button rate-button">
			<span class="dashicons dashicons-star-filled"></span> 
			<?php _e('Rate us (5 stars?)', 'companion-sitemap-generator'); ?>
		</a>
		<a href="<?php echo csg_donateUrl(); ?>" target="_blank" class="csg-button donate-button">
			<span class="dashicons dashicons-heart"></span> 
			<?php _e('Donate to help development', 'companion-sitemap-generator'); ?>
		</a>
		<p style="font-size: 12px; color: #BDBDBD;">Donations via PayPal. Amount can be changed.</p>
	</div>

</div>
