<div class="welcome-to-csg support-bg">
	<div class="welcome-column welcome-column-first welcome-column welcome-column-half">
		<div class="welcome-column welcome-column welcome-column-third">
			<h3><?php _e( 'Help' ); ?></h3>
			<ul>
				<li><a href="#" target="_blank"><?php _e( 'Frequently Asked Questions', 'companion-sitemap-generator' ); ?></a></li>
				<li><a href="https://wordpress.org/support/plugin/companion-sitemap-generator" target="_blank"><?php _e( 'Support Forums' ); ?></a></li>
			</ul>
		</div><div class="welcome-column welcome-column welcome-column-third">
			<h3><?php _e( 'Want to contribute?', 'companion-sitemap-generator' ); ?></h3>
			<ul>
				<li><a href="http://codeermeneer.nl/contact/" target="_blank"><?php _e( 'Give feedback', 'companion-sitemap-generator' ); ?></a></li>
				<li><a href="https://translate.wordpress.org/projects/wp-plugins/companion-sitemap-generator/" target="_blank"><?php _e( 'Help us translate', 'companion-sitemap-generator' ); ?></a></li>
			</ul>
		</div><div class="welcome-column welcome-column welcome-column-third">
			<h3><?php _e( 'Developer?', 'companion-sitemap-generator' ); ?></h3>
			<ul>
				<li><a href="https://codeermeneer.nl/documentation/sitemap-generator/" target="_blank"><?php _e( 'Documentation' ); ?></a></li>
			</ul>
		</div>
	</div>
</div>

<div class="welcome-to-csg welcome-bg">
	<div class="welcome-column welcome-column-first welcome-column welcome-column-half">
		<h3><?php _e('Support', 'companion-sitemap-generator');?></h3>
		<p><?php _e('Feel free to reach out to us if you have any questions or feedback.', 'companion-sitemap-generator'); ?></p>
		<a href="https://codeermeneer.nl/contact/" target="_blank" class="button"><?php _e( 'Contact us', 'companion-sitemap-generator' ); ?></a> 
		<a href="https://codeermeneer.nl/plugins/" target="_blank" class="minimal-button"><?php _e('Check out our other plugins', 'companion-sitemap-generator');?></a>
	</div><div class="welcome-column welcome-column-half">
		
	</div>
</div>

<div class="welcome-to-csg love-bg csg-show-love">
	<div class="welcome-column welcome-column-first welcome-column welcome-column-half">
		<h3><?php _e( 'Like our plugin?', 'companion-sitemap-generator' ); ?></h3>
		<p><?php _e('Companion Sitemap Generator is free to use. It has required a great deal of time and effort to develop and you can help support this development by making a small donation.<br />You get useful software and we get to carry on making it better.', 'companion-sitemap-generator'); ?></p>
	</div><div class="welcome-column welcome-column welcome-column-half">
		<a href="https://wordpress.org/support/plugin/companion-sitemap-generator/reviews/#new-post" target="_blank" class="csg-button rate-button">
			<span class="dashicons dashicons-star-filled"></span> 
			<?php _e('Rate us (5 stars?)', 'companion-sitemap-generator'); ?>
		</a><a href="<?php echo csg_donateUrl(); ?>" target="_blank" class="csg-button donate-button">
			<span class="dashicons dashicons-heart"></span> 
			<?php _e('Donate to help development', 'companion-sitemap-generator'); ?>
		</a>
		<p style="font-size: 12px; color: #BDBDBD;">Donations via PayPal. Amount can be changed.</p>
	</div>
</div>